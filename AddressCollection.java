package com.innominds.flipazon.AC;

//Java dependencies
import java.util.HashSet;

import java.util.Iterator;

import com.innominds.fip.A.Address;

/**
* This is a wrapper class to contain all the address in the 
* Flipazon application.
* @author jpalepally1
*/
public class AddressCollection {
	
	private static HashSet<Address> listOfAllProducts;
	
	/** Constructor */
	public AddressCollection() {
		listOfAllProducts = new HashSet<Address>();
	}
	
	/**
	 * This is a helper method, to create a Address object.
	 * @param h_no
	 * @param street
	 * @param pincode
	 * @param city
	 * @param country
	 * @author jpalepally1
	 */
	public boolean addAddress(String Houseno, String Street,String district,String Pincode,String city,String Country) {

		try {
			Address newAddress = new Address(Houseno,Street,district,Pincode,city,Country, Country);
			listOfAllProducts.add(newAddress);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	
	/**
	 * This is a helper method, to print all address in collection 
	 * in human readable format.
	 */
	public void printAllProducts() {
		for (Iterator<Address> iter = listOfAllProducts.iterator(); iter.hasNext();) {
		     Address currentProduct = (Address) iter.next();
			System.out.println(currentProduct);
		}
	}
	
	/**
	 * Helper method to print the set of address specified as parameter
	 * @param results a HashSet<Address> to be printed in human readable format
	 */
	public void printTheseProducts(HashSet<Address> results) {
		
		for (Address currProduct : results) {
			System.out.println(currProduct);
		}
	}
	
	/**
	 * Helper method to accept search string to search for Address matching string.
	 * @param searchString
	 * @return A Set of address matching the searchString
	 */
	public HashSet<Address> returnSearchResultsProducts(String searchString) {
		HashSet<Address> returnSet = new HashSet<Address>();
		
		for (Address currProduct : listOfAllProducts) {
			System.out.println(currProduct.getHouseno());
			System.out.println(currProduct.getStreet());
			System.out.println(currProduct.getPincode());
			System.out.println(currProduct.getCity());
			System.out.println(currProduct.getCountry());

			System.out.println(searchString);
			
			if ((currProduct.getHouseno().contains(searchString) ||
					currProduct.getStreet().contains(searchString))) {
				returnSet.add(currProduct);
				System.out.println("Matched!");
			} else {
				System.out.println("Mismatch!");
			}
		}	
		return returnSet;
	}

	/**
	 * Helper method to find a address by its state
	 * @param state	
	 * @return the PRoduct object if found, or null object if not found
	 */
	public Address getProductById(String searchID) {
		
		for (Address currProduct : listOfAllProducts) {
			if (currProduct.getPincode().matches(searchID)) {
				return currProduct;
			} 
		}
		return null;
	}
}