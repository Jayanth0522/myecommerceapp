package com.innominds.fip.A;
/**
 * This is the implementation of Address class 
 * @author jpalepally1
 *
 */
public class Address {
	 String Houseno;
	 String Street;
	 String district;
	 String city;
	 String Pincode;
	 String Country;
	// Creation of a constructor
	public Address(String Houseno, String Street,String district, String city,String Country,String Pincode, String string){
	 this.Houseno	= Houseno;
	 this.Street 	= Street;
	 this.district	= district;
	 this.city   	= city;
	 this.Country	= Country;
	 this.Pincode	= Pincode;
	}
	// Creation of tostring method
    public String tostring() {
     String StringToReturn = "";
     StringToReturn += "Houseno  :      "+this.Houseno+			 "\n";
     StringToReturn += "Street   :      "+this.Street+           "\n";
     StringToReturn += "district :      "+this.district+         "\n";
     StringToReturn += "city     :      "+this.city+             "\n";
     StringToReturn += "Country  :      "+this.Country+          "\n"; 
     StringToReturn += "Pincode  :      "+this.Pincode+          "\n";
     return StringToReturn ;
    }
	public String getHouseno() {
		return Houseno;
	}
	public String getStreet() {
		return Street;
	}
	public String getdistrict() {
		return district;
	}
	public String getCity() {
		return city;
	}
	public String getPincode() {
		return Pincode;
	}
	public String getCountry() {
		return Country;
	}
    
}