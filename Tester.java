package com.innominds.fip;


import java.util.HashSet;

//import com.innominds.fip.A.Address;
import com.innominds.flipozon.Product;
import com.innominds.flipozon.dependencies.Rating;
import com.onlineshopping.Productcollections;
/**
 * Test class. Contains main method to test various features during development
 * @author jpalepally1
 */
public class Tester{
/**
	 * Main method to test various features
	 */
	public static void main(String[] args) throws Exception {
		
		Tester testerObject = new Tester();
		
		testerObject.testProductCreation();

		testerObject.testRatingCreation();
		
		testerObject.testAddressCreation();

	}
	
	/**
	 * Instance method to test Product object creation
	 */
	private void testProductCreation() {

		Productcollections pc = new Productcollections();
		
		pc.addProduct("Adidas Shoes 1", 7000f, "fantastic adidas shoes, blue color");
		pc.addProduct("Adidas Shoes 2", 7099f, "fantastic adidas shoes, red color");
		pc.addProduct("Adidas Shoes 3", 8000f, "fantastic adidas shoes, black color");
		
		 pc.printAllProducts();
		
		 HashSet<Product> results = pc.returnSearchResultsProducts("red");
		 System.out.println(results.size());
		// pc.printTheseProducts(results);
		// Product product4567 = pc.getProductById("4567");
		
		//product48496.addRating(4, "This is awesome product", "12356");
	}
	
	/**
	 * Instance method to test Rating object creation
	 */
	private void testRatingCreation() {
		
		try {
			// testing rating creation
			Rating myrating = new Rating("100", (byte) 2, "this is good ");
			System.out.println(myrating);
			Rating mysecondrating = new Rating("10098", (byte) 3, "this");
			System.out.println(mysecondrating);
			Rating mythirdrating = new Rating("10098", (byte) 5, "this is notgood");
			System.out.println(mythirdrating);
			
			} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
		}
	}

	/**
	 * Instance method to test Address object creation
	 */
	private void testAddressCreation() {

		try {
			// testing address creation
		//	Address newAddress =  new Address("12-54","Nacharam","Medchal","Hyderabad","India","5000039", null);
			
			} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
		}
	}

}